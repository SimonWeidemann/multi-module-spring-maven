# Introduction
The Basic Idea behind this Project is to show how a Multi Module Web Application can be setup. 
It is separated into Five Modules: Application, Core, Infrastructure, Input, Output.

# Application
The Application is the point where all Strings run together.
It includes a dependency on each Module to build up the System and start the App.

# Infrastructure
The Infrastructure Module has no Dependencies on other Modules and contains only the Domain Models and Interfaces.
It can be added to each module and, because it has no dependencies, there will be no dependency circles.
The Module where it is added does not need a Dependency on the Module which implements the Interface.
This dependency resolution will be done by Spring injection.

# Core
This is the Layer which connects OUTPUT and INPUT.
It enriches the data, modifies it, routes it etc.
It handles Business Logic.

# Output
The Module has an example Implementation of a Client, which gets Data from the Web.

# Input
The Module has an example Implementation of a Controller to get some Items.

# Pros
Dependency Management
- More granular, each Module can have its own dependency

Visibility
- Each Module can have its own Models which are only visible inside the Module.
- Namespaces are not polluted by Model DTO  Classes, or other Services etc.
- Structures the Repository

Borders
- Forced Separation of Concerns

Flexibility
- For example, replacing the implementation of Interface
- Functionality can easily be shared.

Testing
- It can be tested versus the Interfaces and not Implementation.

# Cons

More Code

Dependency Management

Needs some time to Set up

# Try it out

Run local

## Example curl 

```shell
curl --location --request GET 'http://localhost:8080/items' \
--header 'Content-Type: application/json' \
--data-raw '{
   "number": 10
}'
```
