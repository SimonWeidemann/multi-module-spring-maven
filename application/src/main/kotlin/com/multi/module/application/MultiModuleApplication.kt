package com.multi.module.application

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["com.multi.module"])
open class MultiModuleApplication

fun main(vararg args: String) {
        runApplication<MultiModuleApplication>(*args)
}