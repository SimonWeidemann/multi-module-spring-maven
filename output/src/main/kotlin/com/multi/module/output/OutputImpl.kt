package com.multi.module.output

import com.multi.module.infrastructure.interfaces.Output
import com.multi.module.infrastructure.models.ToDo
import com.multi.module.output.client.TodosClient
import com.multi.module.output.models.ToDoDto
import org.springframework.stereotype.Component

@Component
internal class OutputImpl(
    private val todosClient : TodosClient
) : Output {

    override suspend fun downloadItem(): List<ToDo> =
        todosClient
            .getToDos()
            .map { ToDo(it.id,it.title, it.completed)}

}
