package com.multi.module.output.client

import com.multi.module.output.models.ToDoDto
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody

@Service
internal class TodosClient(
    webClient: WebClient.Builder
)
{
    private val client = webClient.build()

    suspend fun getToDos() =
        client
            .get()
            .uri("https://jsonplaceholder.typicode.com/todos")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .awaitBody<List<ToDoDto>>()
}