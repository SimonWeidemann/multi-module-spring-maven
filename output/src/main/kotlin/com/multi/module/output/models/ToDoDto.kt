package com.multi.module.output.models

internal data class ToDoDto(
    val userId: Int,
    val id: Int,
    val title: String,
    val completed: Boolean
)