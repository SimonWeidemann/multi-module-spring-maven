package com.multi.module.infrastructure.interfaces

import com.multi.module.infrastructure.models.ToDo

interface Input {
    suspend fun getItems(amount : Int): List<ToDo>
}