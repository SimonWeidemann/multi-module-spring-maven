package com.multi.module.infrastructure.interfaces

import com.multi.module.infrastructure.models.ToDo

interface Output {
   suspend fun downloadItem() : List<ToDo>
}