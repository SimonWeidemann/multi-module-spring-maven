package com.multi.module.infrastructure.models

data class ToDo(
val id: Int,
val title: String,
val completed: Boolean
)