package com.multi.module.input.models

data class Amount(val number: Int)