package com.multi.module.input.controller

import com.multi.module.infrastructure.interfaces.Input
import com.multi.module.infrastructure.models.ToDo
import com.multi.module.input.models.Amount
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/items")
@RestController
internal class ItemsController(
    private val input: Input,
) {

    @GetMapping(
        "",
        produces = [MediaType.APPLICATION_JSON_VALUE],
    )
    suspend fun getItems(@RequestBody amount: Amount): List<ToDo> = input.getItems(amount.number)

}