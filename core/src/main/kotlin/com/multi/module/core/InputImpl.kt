package com.multi.module.core

import com.multi.module.infrastructure.interfaces.Input
import com.multi.module.infrastructure.interfaces.Output
import com.multi.module.infrastructure.models.ToDo
import org.springframework.stereotype.Component

@Component
internal class InputImpl(
    private val output: Output
) : Input {

    override suspend fun getItems(amount: Int): List<ToDo> = output
        .downloadItem()
        .subList(0, if(amount < 200) amount else 200)

}